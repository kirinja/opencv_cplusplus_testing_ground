#include <opencv2/xfeatures2d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <ctime>
#include <iostream>
#include <opencv2/tracking/tracker.hpp>

float SCALE_FACTOR = 1;

// https://docs.opencv.org/3.3.1/d7/dff/tutorial_feature_homography.html
void SurfTutorial(std::string input_scene)
{
	cv::Mat img_object = cv::imread("A_image_to_find.png", cv::IMREAD_GRAYSCALE);
	cv::Mat img_scene = cv::imread(input_scene, cv::IMREAD_GRAYSCALE);

	clock_t Start = clock();

	// step 1: detect the keypoints and extract descriptors using SURF
	cv::Ptr<cv::xfeatures2d::SURF> detector = cv::xfeatures2d::SURF::create(500);

	std::vector<cv::KeyPoint> keypoints_object, keypoints_scene;
	cv::Mat descriptors_object, descriptors_scene;

	detector->detectAndCompute(img_object, cv::Mat(), keypoints_object, descriptors_object);
	detector->detectAndCompute(img_scene, cv::Mat(), keypoints_scene, descriptors_scene);

	// step 2: Matching descriptor vectors using FLANN matcher
	cv::FlannBasedMatcher matcher;
	std::vector<cv::DMatch> matches;

	matcher.match(descriptors_object, descriptors_scene, matches);
	double max_dist = 0; double min_dist = 100;

	// quick calculation of max and min distance between keypoints
	for (int i = 0; i < descriptors_object.rows; i++)
	{
		double dist = matches[i].distance;
		if (dist < min_dist) min_dist = dist;
		if (dist > max_dist) max_dist = dist;
	}

	printf("-- Max dist : %f\n", max_dist);
	printf("-- Min dist : %f\n", min_dist);

	// draw only good matches (i.e. whose distance is less than 3*min_dist
	std::vector<cv::DMatch> good_matches;
	for (int i = 0; i < descriptors_object.rows; i++)
	{
		if (matches[i].distance < 3 * min_dist)
		{
			good_matches.push_back(matches[i]);
		}
	}

	cv::Mat img_matches;
	cv::drawMatches(img_object, keypoints_object, img_scene, keypoints_scene, good_matches,
		img_matches, cv::Scalar::all(-1), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

	// localize the object
	std::vector<cv::Point2f> obj;
	std::vector<cv::Point2f> scene;

	for (size_t i = 0; i < good_matches.size(); i++)
	{
		// get the keypoints from the good matches
		obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
		scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
	}

	if (obj.size() != 0 && scene.size() != 0) {
		cv::Mat H = cv::findHomography(obj, scene, cv::RANSAC);

		// get the corners of the image_1 (the object to be "detected")
		std::vector<cv::Point2f> obj_corners(4);
		obj_corners[0] = cv::Point(0, 0);
		obj_corners[1] = cv::Point(img_object.cols, 0);
		obj_corners[2] = cv::Point(img_object.cols, img_object.rows);
		obj_corners[3] = cv::Point(0, img_object.rows);

		std::vector<cv::Point2f> scene_corners(4);
		cv::perspectiveTransform(obj_corners, scene_corners, H);
	

	// Draw lines between the corners (the mapped object in the scene - image_2)
	cv::line(img_matches, scene_corners[0] + cv::Point2f(img_object.cols, 0), scene_corners[1] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
	cv::line(img_matches, scene_corners[1] + cv::Point2f(img_object.cols, 0), scene_corners[2] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
	cv::line(img_matches, scene_corners[2] + cv::Point2f(img_object.cols, 0), scene_corners[3] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
	cv::line(img_matches, scene_corners[3] + cv::Point2f(img_object.cols, 0), scene_corners[0] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
	}
	// show the detected matches
	cv::imshow("Good matches & object detection", img_matches);
	std::cout << "frametime: " << clock() - Start << std::endl;
	cv::waitKey(0);
}

// kinda works per frame but it's not very good at finding the object (possible the image itself is bad and need to be more complex?)
void SurfTutorialVideoFileTest(std::string object_name, std::string scene_name)
{
	std::vector<float> frametimes;
	cv::VideoCapture capture(scene_name);
	if (!capture.isOpened())
		return;
	
	cv::Mat img_object = cv::imread(object_name, cv::IMREAD_GRAYSCALE);
	//cv::Mat img_scene = cv::imread("A_image_scene.png", cv::IMREAD_GRAYSCALE);
	cv::Mat img_scene;

	bool running = true;

	while (running)
	{
		clock_t Start = clock();

		// instead of doing this for every frame we do it for the first frame, then try to track the object (look in facesource.cpp for tracking code (DetectObject() ))
		// if we lose track of the object then we try to find it again using SURF
		// basically flow will be:
		// 0. if we already found object then dont do SURF, instead skip to tracking part
		// 1. try to find object using SURF
		// 2. if cant find object then skip frame
		// 3. if we have found object then set tracking
		// 4. if we lose track then try to find object again using SURF

		capture >> img_scene;
		if (img_scene.empty())
			break;
		
		// resize image to make it smaller, to increase performance
		cv::Mat resizedGray;
		// Scale down for better performance.
		cv::resize(img_scene, resizedGray, cv::Size(img_scene.cols / SCALE_FACTOR, img_scene.rows / SCALE_FACTOR));
		//cv::equalizeHist(resizedGray, resizedGray);
		
		// step 1: detect the keypoints and extract descriptors using SURF
		cv::Ptr<cv::xfeatures2d::SURF> detector = cv::xfeatures2d::SURF::create();

		std::vector<cv::KeyPoint> keypoints_object, keypoints_scene;
		cv::Mat descriptors_object, descriptors_scene;

		detector->detectAndCompute(img_object, cv::Mat(), keypoints_object, descriptors_object);
		detector->detectAndCompute(resizedGray, cv::Mat(), keypoints_scene, descriptors_scene);

		// step 2: Matching descriptor vectors using FLANN matcher
		cv::FlannBasedMatcher matcher;
		std::vector<cv::DMatch> matches;

		matcher.match(descriptors_object, descriptors_scene, matches);
		double max_dist = 0; double min_dist = 100;

		// quick calculation of max and min distance between keypoints
		for (int i = 0; i < descriptors_object.rows; i++)
		{
			double dist = matches[i].distance;
			if (dist < min_dist) min_dist = dist;
			if (dist > max_dist) max_dist = dist;
		}

		//printf("-- Max dist : %f\n", max_dist);
		//printf("-- Min dist : %f\n", min_dist);

		// draw only good matches (i.e. whose distance is less than 3*min_dist
		std::vector<cv::DMatch> good_matches;
		for (int i = 0; i < descriptors_object.rows; i++)
		{
			if (matches[i].distance < 3 * min_dist)
			{
				good_matches.push_back(matches[i]);
			}
		}

		cv::Mat img_matches;
		cv::drawMatches(img_object, keypoints_object, resizedGray, keypoints_scene, good_matches,
			img_matches, cv::Scalar::all(-1), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

		// localize the object
		std::vector<cv::Point2f> obj;
		std::vector<cv::Point2f> scene;

		for (size_t i = 0; i < good_matches.size(); i++)
		{
			// get the keypoints from the good matches
			obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
			scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
		}

		if (obj.size() >= 4 && scene.size() >= 4) {
			cv::Mat H = cv::findHomography(obj, scene, cv::RANSAC);
			if (H.cols > 0 && H.rows > 0) {

				// get the corners of the image_1 (the object to be "detected")
				std::vector<cv::Point2f> obj_corners(4);
				obj_corners[0] = cv::Point(0, 0);
				obj_corners[1] = cv::Point(img_object.cols, 0);
				obj_corners[2] = cv::Point(img_object.cols, img_object.rows);
				obj_corners[3] = cv::Point(0, img_object.rows);

				std::vector<cv::Point2f> scene_corners(4);
				cv::perspectiveTransform(obj_corners, scene_corners, H);


				// Draw lines between the corners (the mapped object in the scene - image_2)
				// these 4 lines create the rectangle which is printed on screen, Scalar is the color of the rectangle (GREEN), 4 is line thickness
				cv::line(img_matches, scene_corners[0] + cv::Point2f(img_object.cols, 0), scene_corners[1] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
				cv::line(img_matches, scene_corners[1] + cv::Point2f(img_object.cols, 0), scene_corners[2] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
				cv::line(img_matches, scene_corners[2] + cv::Point2f(img_object.cols, 0), scene_corners[3] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
				cv::line(img_matches, scene_corners[3] + cv::Point2f(img_object.cols, 0), scene_corners[0] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
			}
		}
		// show the detected matches
		cv::imshow("Good matches & object detection", img_matches);

		frametimes.push_back(clock() - Start);
		//std::cout << "frametime: " << clock() - Start << std::endl;
		cv::waitKey(1);
	}
	float frametimes_sum = 0;
	for (float element : frametimes)
	{
		frametimes_sum += element;
	}
	float avg_frametime = frametimes_sum / frametimes.size();
	std::cout << "Average frametime: " << avg_frametime << std::endl;
	//std::cout << "PRESS ANY KEY TO CONTINUE WITH NEXT TEST" << std::endl;
	std::cout << std::endl;
	//cv::waitKey(0);
}

// kinda works per frame but it's not very good at finding the object (possible the image itself is bad and need to be more complex?)
void SurfTutorialVideoPlusTracking()
{
	cv::VideoCapture capture("movie.mp4");
	if (!capture.isOpened())
		return;

	cv::Mat img_object = cv::imread("A_image_to_find.png", cv::IMREAD_GRAYSCALE);
	//cv::Mat img_scene = cv::imread("A_image_scene.png", cv::IMREAD_GRAYSCALE);
	cv::Mat img_scene;

	capture >> img_scene;

	// step 1: detect the keypoints and extract descriptors using SURF
	cv::Ptr<cv::xfeatures2d::SURF> detector = cv::xfeatures2d::SURF::create();

	std::vector<cv::KeyPoint> keypoints_object, keypoints_scene;
	cv::Mat descriptors_object, descriptors_scene;

	detector->detectAndCompute(img_object, cv::Mat(), keypoints_object, descriptors_object);
	detector->detectAndCompute(img_scene, cv::Mat(), keypoints_scene, descriptors_scene);

	// step 2: Matching descriptor vectors using FLANN matcher
	cv::FlannBasedMatcher matcher;
	std::vector<cv::DMatch> matches;

	matcher.match(descriptors_object, descriptors_scene, matches);
	double max_dist = 0; double min_dist = 100;

	// quick calculation of max and min distance between keypoints
	for (int i = 0; i < descriptors_object.rows; i++)
	{
		double dist = matches[i].distance;
		if (dist < min_dist) min_dist = dist;
		if (dist > max_dist) max_dist = dist;
	}

	printf("-- Max dist : %f\n", max_dist);
	printf("-- Min dist : %f\n", min_dist);

	// draw only good matches (i.e. whose distance is less than 3*min_dist
	std::vector<cv::DMatch> good_matches;
	for (int i = 0; i < descriptors_object.rows; i++)
	{
		if (matches[i].distance < 3 * min_dist)
		{
			good_matches.push_back(matches[i]);
		}
	}

	cv::Mat img_matches;
	cv::drawMatches(img_object, keypoints_object, img_scene, keypoints_scene, good_matches,
		img_matches, cv::Scalar::all(-1), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

	// localize the object
	std::vector<cv::Point2f> obj;
	std::vector<cv::Point2f> scene;

	for (size_t i = 0; i < good_matches.size(); i++)
	{
		// get the keypoints from the good matches
		obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
		scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
	}

	std::vector<cv::Point2f> scene_corners(4);
	std::vector<cv::Point2f> obj_corners(4);

	if (obj.size() >= 4 && scene.size() >= 4) {
		cv::Mat H = cv::findHomography(obj, scene, cv::RANSAC);
		if (H.cols > 0 && H.rows > 0) {

			// get the corners of the image_1 (the object to be "detected")
			obj_corners[0] = cv::Point(0, 0);
			obj_corners[1] = cv::Point(img_object.cols, 0);
			obj_corners[2] = cv::Point(img_object.cols, img_object.rows);
			obj_corners[3] = cv::Point(0, img_object.rows);

			cv::perspectiveTransform(obj_corners, scene_corners, H);


			// Draw lines between the corners (the mapped object in the scene - image_2)
			// these 4 lines create the rectangle which is printed on screen, Scalar is the color of the rectangle (GREEN), 4 is line thickness
			cv::line(img_matches, scene_corners[0] + cv::Point2f(img_object.cols, 0), scene_corners[1] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
			cv::line(img_matches, scene_corners[1] + cv::Point2f(img_object.cols, 0), scene_corners[2] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
			cv::line(img_matches, scene_corners[2] + cv::Point2f(img_object.cols, 0), scene_corners[3] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
			cv::line(img_matches, scene_corners[3] + cv::Point2f(img_object.cols, 0), scene_corners[0] + cv::Point2f(img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
		}
	}

	cv::Rect2d roi;
	roi = cv::Rect2d(scene_corners[0], scene_corners[2]);

	// create tracker object
	cv::Ptr<cv::Tracker> tracker = cv::TrackerKCF::create();

	// init the tracker
	tracker->init(img_scene, roi);

	bool running = true;
	while (running)
	{
		clock_t Start = clock();
		
		// instead of doing this for every frame we do it for the first frame, then try to track the object (look in facesource.cpp for tracking code (DetectObject() ))
		// if we lose track of the object then we try to find it again using SURF
		// basically flow will be:
		// 0. if we already found object then dont do SURF, instead skip to tracking part
		// 1. try to find object using SURF
		// 2. if cant find object then skip frame
		// 3. if we have found object then set tracking
		// 4. if we lose track then try to find object again using SURF
				
		capture >> img_scene;
		if (img_scene.empty())
			break;
						
		// update tracker result
		bool tracked = tracker->update(img_scene, roi); // seems like it isnt moving at the same speed as the object in the scene, making it lag behind and get stuck on something else

		// draw the tracked object
		cv::rectangle(img_scene, roi, cv::Scalar(255, 0, 0), 2, 1);

		// show the image with the tracked object;
		cv::imshow("tracker", img_scene);

		// show the detected matches
		//cv::imshow("Good matches & object detection", img_matches);

		cv::waitKey(64);
		std::cout << "frametime: " << clock() - Start << " \t Object tracked: " << tracked << std::endl;
	}

}

// we could possibly use MSER to detect regions and store these and then compare to a dummy object we have made
// could possibly be faster, we should write about this in the thesis
void MSER_test()
{
	cv::Mat img = cv::imread("A_image_to_find.png", cv::IMREAD_GRAYSCALE);

	cv::Ptr<cv::MSER> ms = cv::MSER::create();
	std::vector<std::vector<cv::Point> > regions;
	std::vector<cv::Rect> mser_bbox;
	ms->detectRegions(img, regions, mser_bbox);

	for (int i = 0; i < regions.size(); i++)
	{
		cv::rectangle(img, mser_bbox[i], CV_RGB(0, 255, 0));
	}

	cv::imshow("mser", img);
}

int main()
{
	std::cout << "--------------------------" << std::endl;
	std::cout << "Starting tests with smaller object image" << std::endl;

	SurfTutorialVideoFileTest("A_image_to_find_small.png", "movie_small.mp4");
	SurfTutorialVideoFileTest("A_image_to_find_small.png", "movie_medium.mp4");
	SurfTutorialVideoFileTest("A_image_to_find_small.png", "movie_large.mp4");

	std::cout << "--------------------------" << std::endl;
	std::cout << "Starting tests with Larger object image" << std::endl;

	SurfTutorialVideoFileTest("A_image_to_find_large.png", "movie_small.mp4");
	SurfTutorialVideoFileTest("A_image_to_find_large.png", "movie_medium.mp4");
	SurfTutorialVideoFileTest("A_image_to_find_large.png", "movie_large.mp4");

	/*SurfTutorial("A_image_scene_m.png");
	SurfTutorial("A_image_scene_s.png");
	SurfTutorial("A_image_scene_x.png");*/
	

	//MSER_test();

	// this doesnt work perfectly since it will lose track of the object after a while
	// maybe do a check if the object is correctly tracked every x seconds or something?
	//SurfTutorialVideoPlusTracking();

	cv::waitKey(0);
	return 0;
}